#!/usr/bin/env python

import click
import math
from numpy import random

location_throughput = 200

@click.command()
@click.option('--seed', type=int, required = True, default=0)
@click.option('--n-seasons', type=int, required = True, default=8)
@click.option('--n-days', type=int, required = True, default=365)
@click.option('--n-locations', type=int, required = True, default=10)
@click.option('--n-rentals', type=int, required = True, default=7500)
@click.option('--n-products', type=int, required = True, default=50)
@click.option('--n-vtypes', type=int, required = True, default=150)
@click.option('--n-vehicles', type=int, required = True, default=1500)
@click.option('--maint-prob', type=float, required = True, default=0.2)
@click.option('--disp-prob', type=float, required = True, default=0.2)
@click.option('--veh-dailydelaycostmean', type=float, required = True, default=200)
@click.option('--veh-dailydelaycostsd', type=float, required = True, default=50)
@click.option('--rent-lengthmean', type=float, required = True, default=15)
@click.option('--rent-lengthsd', type=float, required = True, default=5)
@click.option('--rent-dailypricemean', type=float, required = True, default=200)
@click.option('--rent-dailypricesd', type=float, required = True, default=50)
@click.option('--reloc-durationmean', type=float, required = True, default=10)
@click.option('--reloc-durationsd', type=float, required = True, default=5)
@click.option('--reloc-costmean', type=float, required = True, default=2500)
@click.option('--reloc-costsd', type=float, required = True, default=500)
@click.option('--reloc-missingprob', type=float, required = True, default=0.1)
@click.option('--subst-tadurationmean', type=float, required = True, default=1)
@click.option('--subst-tadurationsd', type=float, required = True, default=1)
@click.option('--subst-tacostmean', type=float, required = True, default=150)
@click.option('--subst-tacostsd', type=float, required = True, default=50)
@click.option('--subst-costmean', type=float, required = True, default=50)
@click.option('--subst-costsd', type=float, required = True, default=25)
def main(seed, n_seasons, n_days, n_locations, n_rentals, n_products,
        n_vtypes, n_vehicles, maint_prob, disp_prob, veh_dailydelaycostmean,
        veh_dailydelaycostsd, rent_lengthmean, rent_lengthsd,
        rent_dailypricemean, rent_dailypricesd, reloc_durationmean,
        reloc_durationsd, reloc_costmean, reloc_costsd, reloc_missingprob,
        subst_tadurationmean, subst_tadurationsd, subst_tacostmean,
        subst_tacostsd, subst_costmean, subst_costsd):

    anywhere = "ANY"
    random.seed(seed)

    # locations
    locations = [
        {
            'name': 'L%d' % i,
            'max_checkout': 200,
            'throughput_penalty': 0.0
        } for i in range(n_locations)
    ]

    print '# location NAME MAX_CHECKOUT/IN THROUGPUT_PENALTY'
    for l in locations:
        print 'location', l['name'], l['max_checkout'], l['throughput_penalty']

    # products
    products = [ 'P%d' % i for i in range(n_products)]

    print ''
    print '# product NAME'
    for p in products:
        print 'product', p

    # vehicle types
    vehicle_types = [
        {
            'name': 'VT%d' % i,
            'is_local': False,
            'daily_run_cost': 0.0,
            'daily_delay_cost': max(
                100.0,
                round(random.normal(veh_dailydelaycostmean, veh_dailydelaycostsd))
            )
        } for i in range(n_vtypes) ]

    print ''
    print '# vehtype NAME IS_LOCAL DAILY_RUN_COST DAILY_DELAY_COST'
    for vt in vehicle_types:
        print 'vehtype', vt['name'], int(vt['is_local']), vt['daily_run_cost'], vt['daily_delay_cost']

    # relocation schemes
    relocations = []
    for l1 in locations:
        for l2 in locations:
            if l1 == l2 or random.random() < reloc_missingprob:
                continue

            durations = [ int(max(0, random.normal(reloc_durationmean, reloc_durationsd))) for i in range(3) ]
            durations.sort()

            costs = [ round(max(0, random.normal(reloc_costmean, reloc_costsd))) for i in range(3) ]
            costs.sort()
            costs.reverse()

            for i in range(3):
                relocations.append({
                    'from': l1['name'],
                    'to': l2['name'],
                    'duration': durations[i],
                    'cost': costs[i],
                    'applies_to_availability': int(random.random() < 0.5),
                    'start_date': 0,
                    'end_date': n_days * 2 + 1
                })

    print ''
    print '# reloc FROM TO DURATION COST APPLIES_TO_AVAILABILITY START_DATE END_DATE'
    for r in relocations:
        print 'reloc', r['from'], r['to'], r['duration'], r['cost'], r['applies_to_availability'], r['start_date'], r['end_date']

    # substitution schemes
    substitutions = []
    vt_per_product = int(math.ceil(float(n_vtypes) / n_products))
    print vt_per_product
    for pi in range(n_products):

        vt_per_this_product = max(1, random.poisson(vt_per_product))

        vt_start = min(vt_per_this_product * pi, n_vtypes-1)
        vt_end = min(vt_per_this_product * (pi + 1), n_vtypes)

        if pi > 0:
            vt_start = min(vt_per_this_product * (pi - 1), n_vtypes-1)

        if pi < n_products-1:
            vt_end = min(vt_per_this_product * (pi + 2), n_vtypes)

        for vti in range(vt_start, vt_end):

            durations = [ int(max(0, random.normal(subst_tadurationmean, subst_tadurationsd))) for i in range(3) ]
            durations.sort()

            costs = [ round(max(0, random.normal(subst_tacostmean, subst_tacostsd))) for i in range(2) ]
            costs.append(0)
            costs.sort()
            costs.reverse()

            for i in range(3):
                substitutions.append({
                    'vehicle_type': vehicle_types[vti]['name'],
                    'product': products[pi],
                    'fixed_cost': 0.0,
                    'daily_cost': round(max(0.0, random.normal(subst_costmean, subst_costsd))),
                    'visibility_level': 1,
                    'applies_to_availability':  int(random.random() < 0.5),
                    'tac_number_of_half_days': durations[i],
                    'tac_cost': costs[i],
                    'start_date': 0,
                    'end_date': n_days * 2 + 1
                })

    print ''
    print '# subst1 VEH_TYPE PRODUCT FIXED_COST DAILY_COST VISIBILITY_LEVEL APPLIES_TO_AVAILABILITY TAC_NUMBER_OF_HALF_DAYS TAC_COST START_DATE END_DATE'
    for s in substitutions:
        print 'subst1', s['vehicle_type'], s['product'], s['fixed_cost'], s['daily_cost'], s['visibility_level'], s['applies_to_availability'], s['tac_number_of_half_days'], s['tac_cost'], s['start_date'], s['end_date']

    # vehicles
    vehicles = []
    days_per_season = n_days / n_seasons
    for vi in range(n_vehicles):
        avail_date = 0
        if random.random() < 0.1:
            season = min(random.poisson(1), n_seasons-1)
            season_start = days_per_season * season
            season_end = days_per_season * (season + 1)
            avail_date = random.randint(season_start, season_end)

        vehicles.append({
            'id': 'V%d' % vi,
            'vehicle_type': vehicle_types[random.randint(0, len(vehicle_types))]['name'],
            'year': min(int(random.normal(2017, 3)), 2017),
            'location': locations[random.randint(0, len(locations))]['name'],
            'avail_date': avail_date,
            'avail_period': int(random.random() < 0.5 * (avail_date > 0))
        })

    print ''
    print '# vehicle UNIT_NO VEHICLE_TYPE YEAR LOCATION AVAIL_DATE AVAIL_PERIOD'
    for v in vehicles:
        print 'vehicle', v['id'], v['vehicle_type'], v['year'], v['location'], v['avail_date'], v['avail_period']

    # rental requests
    rentals = []
    for ri in range(n_rentals):

        duration = max(3, int(random.normal(rent_lengthmean, rent_lengthsd)))
        daily_price = max(30, int(random.normal(rent_dailypricemean, rent_dailypricesd)))
        from_date = random.randint(0, n_days - (duration / 2))
        to_date = from_date + duration

        rentals.append({
            'id': 'R%d' % ri,
            'product': products[random.randint(0, n_products)],
            'from_location': locations[random.randint(0, n_locations)]['name'],
            'from_date': from_date,
            'from_period': int(random.random() < 0.5),
            'to_location': locations[random.randint(0, n_locations)]['name'],
            'to_date': to_date,
            'to_period': int(random.random() < 0.5),
            'revenue': random.poisson(duration * daily_price),
            'priority': 0,
            'allowed': '-'
        })

    print ''
    print '# rental RENT_ID PRODUCT FROM_LOC FROM_DATE FROM_PERIOD TO_LOC TO_DATE TO_PERIOD REVENUE PRIORITY ALLOWED_VEHICLES'
    for r in rentals:
        print 'rental', r['id'], r['product'], r['from_location'], r['from_date'], r['from_period'], r['to_location'], r['to_date'], r['to_period'], r['revenue'], r['priority'], r['allowed']

    # maintenance & disposal requests
    maintenances = []
    disposals = []

    for v in vehicles:
        maintenance = None

        if random.random() < maint_prob:

            season = random.randint(1, n_seasons-1)
            season_start = season * days_per_season
            season_end = season_start + days_per_season
            date = random.randint(season_start, season_end)
            period = int(random.random() < 0.5)

            maintenance = {
                'vehicle': v['id'],
                'location': locations[random.randint(0, n_locations)]['name'],
                'start_date': date,
                'start_period': period,
                'end_date': date+1,
                'end_period': period
            }

            maintenances.append(maintenance)

        disposal = None
        if random.random() < disp_prob:
            # disposal 00001528 BHO 1 6179 0 00001528

            location = anywhere
            if random.random() < 0.25:
                location = locations[random.randint(n_locations)]['name']

            season = max(1, min(random.poisson(n_seasons-1), n_seasons-2))
            season_start = season * days_per_season
            season_end = season_start + days_per_season
            date = random.randint(season_start, season_end)
            period = int(random.random() < 0.5)

            if maintenance:
                date = random.randint(maintenance['end_date']+1, n_days)

            disposals.append({
                'vehicle': v['id'],
                'location': location,
                'unit_count': 1,
                'date': date,
                'period': period,
                'unit_list': v['id']
            })

    print ''
    print '# disposal VEHICLE LOCATION UNIT_COUNT DATE PERIOD UNIT_LIST'
    for d in disposals:
        print 'disposal', d['vehicle'], d['unit_count'],  d['location'], d['date'], d['period'], d['unit_list']

    print ''
    print '# maint VEHICLE LOCATION START_DATE START_PERIOD END_DATE END_PERIOD'
    for m in maintenances:
        print 'maint', m['vehicle'], m['location'], m['start_date'], m['start_period'], m['end_date'], m['end_period']


if __name__ == '__main__':
    main()
