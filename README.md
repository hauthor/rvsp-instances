# RVSP instance generator

The script `rvsp_gen.py` generates a new instance for the RVSP problem and
dumps it to the screen.

## Usage

The generator can be controlled with a number of parameters, run

    ./rvsp_gen.py --help

to learn about the parameters.

## Existing instances

The folder `instances` contains some pre-generated instances.
